export interface Article {
  id: number;
  href: string;
  title: string;
  subtitle: string;
  date: string;
  author: string;
  category: string;
  categoryLink: string;
  summary: string;
  isDialogOpen: boolean;
}

export interface NewTitle {
  // 編號
  id: number;
  // 標題
  title: string;
}

export interface ArticleList {
  // 修改一筆Article
  modifyArticle(post: NewTitle): any;
  // 刪除一筆Article
  removeArticle(articleID: number): any;
  // 取得 Article所有資料
  getArticles(): Promise<Article[]>;
}
