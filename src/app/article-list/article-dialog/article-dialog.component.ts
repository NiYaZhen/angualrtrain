import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { Article } from '../article-list';
import { FormsModule } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-article-dialog',
  standalone: true,
  imports: [CommonModule, DialogModule, FormsModule, CalendarModule],
  templateUrl: './article-dialog.component.html',
  styleUrls: ['./article-dialog.component.scss'],
})
export class ArticleDialogComponent {
  @Input() isDialogVisibled = false;
  @Input() article!: Article;

  @Output() changes = new EventEmitter<Article>();
  @Output() dialogClosed = new EventEmitter();

  date!: Date;
  originalArticle!: Article;

  ngOnInit(): void {
    this.date = new Date(this.article.date);
    this.originalArticle = { ...this.article };
  }

  doModify() {
    this.article.date = moment(this.date).format('yyyy/MM/DD HH:mm');
    this.isDialogVisibled = false;
    this.article.isDialogOpen = false;
    this.changes.emit(this.article);
  }

  doClose() {
    this.article = { ...this.originalArticle };
    this.isDialogVisibled = false;
    this.dialogClosed.emit();
  }
}
