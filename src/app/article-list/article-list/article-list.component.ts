import { ArticleService } from '../article.service';
import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article, NewTitle } from '../article-list';
import { ArticleHeaderComponent } from '../article-header/article-header.component';
import { ArticleBodyComponent } from '../article-body/article-body.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'his-article-list',
  standalone: true,
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
  imports: [
    CommonModule,
    ArticleHeaderComponent,
    ArticleBodyComponent,
    HttpClientModule,
    FormsModule,
  ],
  providers: [ArticleService],
})
export class ArticleListComponent {
  // 注入 ArticlesService
  articleService: ArticleService = inject(ArticleService);

  // get的資料加上signal
  articles = signal([] as Article[]);

  async ngOnInit() {
    this.articles.set(await this.articleService.getArticles());
  }

  async onModifyArticleTitle(newTitle: NewTitle) {
    try {
      await this.articleService.modifyArticleTitle(newTitle);
      this.articles.mutate((x) => {
        x[x?.findIndex((x) => x.id === newTitle.id)].title = newTitle.title;
      });
    } catch (error) {}
  }

  async onDeleteArticle(article: Article) {
    try {
      await this.articleService.removeArticle(article.id);
      this.articles.mutate((x) => {
        x?.splice(
          x.findIndex((x) => x.id === article.id),
          1,
        );
      });
    } catch (e) {
      console.error(e);
    }
  }
}
