import { Injectable, inject } from '@angular/core';
import { Article, NewTitle } from './article-list';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  #http = inject(HttpClient);

  #url = `http://localhost:3000/articles/`;

  getArticles = async (): Promise<Article[]> => {
    const result$ = this.#http.get<Article[]>(`${this.#url}`);
    return await lastValueFrom(result$);
  };

  removeArticle = async (articleId: number) => {
    const result$ = this.#http.delete(`${this.#url}${articleId}`);
    return await lastValueFrom(result$);
  };

  modifyArticleTitle = async (newTitle: NewTitle) => {
    const result$ = this.#http.patch(`${this.#url}${newTitle.id}`, newTitle);
    return await lastValueFrom(result$);
  };

  modifyArticle = async (article: Article) => {
    const result$ = this.#http.put(
      `${this.#url}${article.id}`,
      article as Article,
    );
    return await lastValueFrom(result$);
  };

  inserArticle = async (article: Article) => {
    const result$ = this.#http.post(`${this.#url}`, article);
    return await lastValueFrom(result$);
  };

  constructor() {}
}
