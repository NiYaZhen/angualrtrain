import { Article, NewTitle } from '../article-list';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  inject,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ArticleDialogComponent } from '../article-dialog/article-dialog.component';
import { ArticleService } from '../article.service';

@Component({
  selector: 'his-article-header',
  standalone: true,
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.scss'],
  imports: [CommonModule, FormsModule, ArticleDialogComponent],
})
export class ArticleHeaderComponent implements OnInit {
  ngOnInit(): void {
    this.originArticle = { ...this.article };
    this.articles.mutate((x) =>
      x.forEach((y) => {
        y.isDialogOpen = false;
      }),
    );
  }

  isDialogVisibled = false;
  // 取父元件 article元素的值
  @Input() article!: Article;

  // originArticle 原始artice（暫存狀態使用）
  originArticle!: Article;

  // 控制 title 和 input開關
  isEdit = false;

  // article要傳入改變資料的值
  NewTitle!: NewTitle;

  articles = signal([] as Article[]);

  articleService: ArticleService = inject(ArticleService);

  // 發射 delete 事件給 父元件
  @Output() delete = new EventEmitter<Article>();
  // 刪除Aritcle
  doDelete() {
    // 發送刪除事件的article資料給父元件
    this.delete.emit(this.article);
  }

  // 發射 changeTitle 事件 給父元件
  @Output() changeTitle = new EventEmitter<any>();
  // 變更article標題
  doChangeTitle() {
    this.NewTitle = {
      id: this.article.id,
      title: this.article.title,
    };
    this.changeTitle.emit(this.NewTitle);
  }

  // 取消編輯
  onCancel() {
    this.article = { ...this.originArticle };
    this.isEdit = false;
  }

  onModify(article: Article) {
    this.articles.mutate((x) => x.forEach((y) => (y.isDialogOpen = false)));
    article.isDialogOpen = true;
  }

  onDialogClosed() {
    this.articles.mutate((x) => x.forEach((y) => (y.isDialogOpen = false)));
  }

  async onModifyArticle(article: Article) {
    try {
      await this.articleService.modifyArticle(article);
      this.articles.mutate((x) => {
        x[x.findIndex((x) => x.id === article.id)] = article;
      });
    } catch (e) {
      console.error(e);
    }
  }
}
