import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ArticleListComponent } from './article-list/article-list/article-list.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    CommonModule,
    RouterOutlet,
    FormsModule,
    HeaderComponent,
    FooterComponent,
    ArticleListComponent,
  ],
})
export class AppComponent {
  keyword = signal<string>('');

  constructor() {}

  public keywordReset() {
    this.keyword.set('');
  }
}
